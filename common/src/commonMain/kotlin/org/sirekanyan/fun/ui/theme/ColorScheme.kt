// This file is generated using https://m3.material.io/theme-builder

package org.sirekanyan.`fun`.ui.theme

import androidx.compose.material3.ColorScheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.ui.graphics.Color

val LightColorScheme: ColorScheme = lightColorScheme(
    primary = Color(0xFF934B00),
    onPrimary = Color(0xFFFFFFFF),
    primaryContainer = Color(0xFFFFDCC5),
    onPrimaryContainer = Color(0xFF301400),
    secondary = Color(0xFF7B4998),
    onSecondary = Color(0xFFFFFFFF),
    secondaryContainer = Color(0xFFF5D9FF),
    onSecondaryContainer = Color(0xFF30004B),
    tertiary = Color(0xFF006493),
    onTertiary = Color(0xFFFFFFFF),
    tertiaryContainer = Color(0xFFCAE6FF),
    onTertiaryContainer = Color(0xFF001E30),
    error = Color(0xFFBA1A1A),
    errorContainer = Color(0xFFFFDAD6),
    onError = Color(0xFFFFFFFF),
    onErrorContainer = Color(0xFF410002),
    background = Color(0xFFFFFBFF),
    onBackground = Color(0xFF201A17),
    surface = Color(0xFFFFFBFF),
    onSurface = Color(0xFF201A17),
    surfaceVariant = Color(0xFFF3DFD2),
    onSurfaceVariant = Color(0xFF52443B),
    outline = Color(0xFF84746A),
    inverseOnSurface = Color(0xFFFBEEE8),
    inverseSurface = Color(0xFF362F2B),
    inversePrimary = Color(0xFFFFB782),
    surfaceTint = Color(0xFF934B00),
    outlineVariant = Color(0xFFD6C3B7),
    scrim = Color(0xFF000000),
)

val DarkColorScheme: ColorScheme = darkColorScheme(
    primary = Color(0xFFFFB782),
    onPrimary = Color(0xFF4F2500),
    primaryContainer = Color(0xFF703800),
    onPrimaryContainer = Color(0xFFFFDCC5),
    secondary = Color(0xFFE5B4FF),
    onSecondary = Color(0xFF491866),
    secondaryContainer = Color(0xFF61317E),
    onSecondaryContainer = Color(0xFFF5D9FF),
    tertiary = Color(0xFF8CCDFF),
    onTertiary = Color(0xFF00344E),
    tertiaryContainer = Color(0xFF004B70),
    onTertiaryContainer = Color(0xFFCAE6FF),
    error = Color(0xFFFFB4AB),
    errorContainer = Color(0xFF93000A),
    onError = Color(0xFF690005),
    onErrorContainer = Color(0xFFFFDAD6),
    background = Color(0xFF201A17),
    onBackground = Color(0xFFECE0DA),
    surface = Color(0xFF201A17),
    onSurface = Color(0xFFECE0DA),
    surfaceVariant = Color(0xFF52443B),
    onSurfaceVariant = Color(0xFFD6C3B7),
    outline = Color(0xFF9F8D83),
    inverseOnSurface = Color(0xFF201A17),
    inverseSurface = Color(0xFFECE0DA),
    inversePrimary = Color(0xFF934B00),
    surfaceTint = Color(0xFFFFB782),
    outlineVariant = Color(0xFF52443B),
    scrim = Color(0xFF000000),
)
