pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "fun"
include(":android", ":desktop", ":common", ":mlkit")
